# Predefined scripts #

I created 2 samples to demonstrate how to build custom widgets based on Universal Gadget for JIRA. I only started to master JavaScript, so please correct mistakes, inaccuracies and bugs you find.

## Weather sample script ##
Actually here I did nothing :-). All credits go to James Fleeting's project: http://simpleweatherjs.com. Please see there for description of parameters passed to simpleWeather. I only put background image from my last vacations and adjusted one line in JavaScript to make it work inside of the gadget. Also you will need to add whitelist entry to allow requests to yahoo weather API https://query.yahooapis.com/v1/public/yql*.

## User timesheet sample script ##
This is kind of more complex script and I used AlmondJS to organize the code. Since version 6.2 JIRA includes AlmondJS in the superbatch but I want the User timesheet sample to work in JIRA 6.0 - 6.1 as well, so I host own copy of the almond.js that is used when JIRA does not provide one.

Short explanation of the code:
**util.js** - util methods.
**eventsProvider.js** - code responsible for fetching worklog data from JIRA. It relies on /rest/api/2/issue/{issueIdOrKey}/worklog and /rest/api/2/search JIRA REST methods.
**EventsProvider.getEvents(dataParams, callBackFunction)** method is an entry point. dataParams is object with input parameters and has following format:

**dataParams.end** - end date exclusively. Please be aware of time part and null it if you want to retrieve worklogs till specific date. To retrieve worklogs including current date, you need to set dataParams.end to midnight of tomorrow. You can have a code like this:

```
#!javascript

dataParams.end = new Date((new Date()).getTime() + (24 * 60 * 60 * 1000));
dataParams.end.setHours(0, 0, 0, 0);
```
**dataParams.start** - start date inclusively. You can use following code to set it 2 weeks back from the end date:

```
#!javascript

dataParams.start = new Date(dataParams.end.getTime() - (14 * 24 * 60 * 60 * 1000));
```
**dataParams.jql** - jql to filter issues. You can omit it to pick all issues. It's better to test it in JIRA issues search before putting here. Sample:

```
#!javascript

dataParams.jql = "status = \"In Progress\"";
```
**dataParams.allUsers** - Pass true value to get worklogs of all users. Overrides dataParams.usernames.
**dataParams.usernames** - Array with usernames. You can use global __gadgetArgs.user object to get username of the currently logged user:

```
#!javascript

dataParams.usernames = [__gadgetArgs.user.username];
```
**callBackFunction** function is called with parameter that is an array of objects that represent worklog entries. Each object has format:

```
#!javascript

{
  "self": "http://www.example.com/jira/rest/api/2/issue/10010/worklog/10000",
  "author": {
    "self": "http://www.example.com/jira/rest/api/2/user?username=fred",
    "name": "fred",
    "displayName": "Fred F. User",
    "active": true
  },
  "updateAuthor": {
    "self": "http://www.example.com/jira/rest/api/2/user?username=fred",
    "name": "fred",
    "displayName": "Fred F. User",
    "active": true
  },
  "comment": "I did some work here.",
  "visibility": {
    "type": "group",
    "value": "jira-developers"
  },
  "started": "2015-07-16T10:12:04.149+0000",
  "startedDate": Date,
  "timeSpent": "3h 20m",
  "timeSpentSeconds": 12000,
  "issueKey": "DEMO-1",
  "id": "100028"
}
```
This is format of the worklog object for JIRA 6.4. Some properties miss in previous JIRA versions.

**tableDrawer.js** - draws the timesheet table based on data from eventsProvider.js The entry method is **tableDrawer.drawByIssue(dataParams, eventsArr, containerJqueryObj)**.

**dataParams** - the same object as for eventsProvider.getEvents.

**eventsArr** - the array of worklog entries.

**containerJqueryObj** - Jquery element to which timesheet table is appended.

tableDrawer.js can be easily replaced by script that draws some charts based on worklog records.
  
Please note that User Timesheet gadget can be slow when many issues with many worklogs have to be fetched from JIRA. Another caveat is that for JIRA before 6.4, eventsProvider relies on issue updated date to filter issues. The code assumes that issue update date is after start date passed in dataParams object.
