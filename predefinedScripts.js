var __predefinedScript = {};
__predefinedScript.weahter = {};
__predefinedScript.weahter.gadgetTitle = "Weather";
__predefinedScript.weahter.jsUrl = "//jira-work-calendar.com/universal-jira-gadget/weather/weather.js";
__predefinedScript.weahter.cssUrl = "//jira-work-calendar.com/universal-jira-gadget/weather/weather.css";
__predefinedScript.weahter.html = '<div id="weather"></div>\n'
    + '<script>\njQuery(document).ready(function() {\n'
    + 'jQuery.simpleWeather({\n'
    + 'location : "Lviv, Ukraine",\n'
    + 'unit : "c",\n'
    + 'success : function(weather) {\n'
    + 'html = "<h2><i class=\\"icon-"+weather.code+"\\"></i> " + weather.temp + "&deg;" + weather.units.temp'
    + '+ "</h2><ul><li>" + weather.city + (weather.region !== "" ? ", " + weather.region : "") + "</li>'
    + '<li class=\\"currently\\">" + weather.currently + "</li>"'
    + '+ "<li>" + weather.wind.direction + " " + weather.wind.speed + " " + weather.units.speed'
    + '+ "</li></ul>";\n'
    + 'for(var i=1;i<weather.forecast.length;i++) {\n'
    + 'html += "<div class=\'forecast-day\'>" + weather.forecast[i].day + " " + weather.forecast[i].date.split(" ")[0] + \n'
    + '"<br/><span class=\'forecast-icon icon-" + weather.forecast[i].code + "\'></span><br/>" + weather.forecast[i].high + "&deg;/"\n'
    + '+ weather.forecast[i].low + "&deg;</div>"; \n' + '}\n'
    + 'jQuery("#weather").html(html);' + '},\n' + 'error : function(error) {\n'
    + 'jQuery("#weather").html("<p>" + error + "</p>");}\n});\n});\n</script>';
__predefinedScript.userTimesheet = {};
__predefinedScript.userTimesheet.gadgetTitle = "User timesheet";
__predefinedScript.userTimesheet.jsUrl = "//jira-work-calendar.com/universal-jira-gadget/common/almond.js, //jira-work-calendar.com/universal-jira-gadget/timesheet/util.js, //jira-work-calendar.com/universal-jira-gadget/timesheet/eventsProvider.js, //jira-work-calendar.com/universal-jira-gadget/timesheet/tableDrawer.js";
__predefinedScript.userTimesheet.cssUrl = "//jira-work-calendar.com/universal-jira-gadget/timesheet/userTimesheet.css";
__predefinedScript.userTimesheet.html = '<div id="container"></div><script>\n'
    + 'define("_ujgJQuery", [], function() {\n' + 'return AJS.$;\n' + '});\n'
    + 'require([ "_ujgJQuery", "_ujgEventsProvider", "_ujgTableDrawer" ], function($, eventsProvider, tableDrawer) {\n'
    + 'var dataParams = {};\n' + '// end date is exclusive, so add 1 day\n'
    + 'dataParams.end = new Date((new Date()).getTime() + (24 * 60 * 60 * 1000))\n'
    + 'dataParams.end.setHours(0, 0, 0, 0);\n' + '// start date is inclusive, set it 1 week back\n'
    + 'dataParams.start = new Date(dataParams.end.getTime() - (15 * 24 * 60 * 60 * 1000));\n'
    + '// dataParams.jql = "status = \"In Progress\"";\n' + '// dataParams.allUsers = true;\n'
    + '// array of usernames\n' + 'dataParams.usernames = [__gadgetArgs.user.username];\n'
    + 'eventsProvider.getEvents(dataParams,\n' + 'function(eventsArr) {\n'
    + 'tableDrawer.drawByIssue(dataParams, eventsArr, $("#container"));\n' + '});\n' + '});\n</script>';