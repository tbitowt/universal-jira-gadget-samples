define("_ujgJQuery", [], function() {
  return jQuery;
});
require([ '_ujgJQuery', '_ujgEventsProvider', '_ujgTableDrawer' ], function($, eventsProvider, tableDrawer) {
  var dataParams = {};
  dataParams.end = new Date('2015-05-01');
  // end date is exclusive
  // dataParams.end.setHours(0, 0, 0, 0);
  // start date is inclusive
  dataParams.start = new Date('2015-04-01'); //new Date(dataParams.end.getTime() - (31 * 24 * 60 * 60 * 1000)); // one month back
  // dataParams.jql = "status = \"In Progress\"";
  // TODO: currentUser true parameter
  dataParams.usernames = [ "admin" ];
  eventsProvider.getEvents(dataParams, function(eventsArr) {
    tableDrawer.drawByIssue(dataParams, eventsArr, $("#container"));
  });
});