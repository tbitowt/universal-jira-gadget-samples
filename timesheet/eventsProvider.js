define(
    '_ujgEventsProvider',
    [ '_ujgJQuery', '_ujgUtil' ],
    function($, util) {
      var EventsProvider = function() {
        return {
          eventsArr : undefined,
          maxResults : 1000, // Could be redefined by JIRA JSON responce
          startAt : undefined,
          total : undefined,
          jiraServerVersion : window.parent.AJS.$('meta[name=ajs-version-number]').attr("content"),

          getEvents : function(dataParams, callbackEventsFun) {
            console.log("getEvents, dataParams = " + dataParams);
            var paramsValidationErrors = "";
            if (dataParams === undefined || typeof dataParams !== 'object') {
              paramsValidationErrors += "You need to pass dataParams object.";
            } else {
              var datesOk = true;
              if (dataParams.start === undefined || !(dataParams.start instanceof Date)) {
                paramsValidationErrors += "\"dataParams.start\" should be valid date. ";
                datesOk = false;
              }
              if (dataParams.end === undefined || !(dataParams.end instanceof Date)) {
                paramsValidationErrors += "\"dataParams.end\" should be valid date. ";
                datesOk = false;
              }
              if (datesOk && dataParams.start >= dataParams.end) {
                paramsValidationErrors += "\"dataParams.start\" should be prior to \"dataParams.end\".";
              }
              if (!dataParams.allUsers
                  && (dataParams.usernames === undefined || !(dataParams.usernames instanceof Array))) {
                paramsValidationErrors += "You need to pass \"dataParams.usernames\" array with usernames or true for \"dataParams.allUsers\".";
              }
            }
            if (paramsValidationErrors !== "") {
              util.showError("EventsProvider.getEvent input parameters errors: " + paramsValidationErrors);
              return;
            }
            if (dataParams.jql === undefined) {
              dataParams.jql = "";
            }
            var that = this;
            // need to "clear" state of the EventsProvider object
            this.eventsArr = [];
            this.startAt = 0;
            this.total = 0;
            this._retrieveJSON(dataParams, function() {
              callbackEventsFun(that.eventsArr);
            });
          },

          _isStrVarSet : function(varToCheck) {
            return varToCheck !== undefined && varToCheck.length > 0;
          },

          _parseDate : function(val) {
            var m = /^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):\d{2}\.\d*(\+|\-)(\d{2})(\d{2})$/.exec(val);
            var minutesOffset = m[7] * 60 + m[8] * 1;
            if (m[6] === '+') {
              minutesOffset = 0 - minutesOffset;
            }
            return new Date(Date.UTC(m[1], m[2] - 1, m[3], m[4], m[5] - 0 + minutesOffset));
          },

          // parses the json response and stores results in evntsObj
          _parseJiraJSON : function(dataParams, json, parseCallback) {
            // console.log(json);
            this.maxResults = json.maxResults;
            this.startAt = json.startAt;
            this.total = json.total;

            if (json.issues.length == 0) {
              // no issues returned
              parseCallback();
              return;
            }

            var i = 0;
            var loopArray = function(json) {
              handleWorklog(json.issues[i], function() {
                i++;
                // any more items in array? continue loop
                if (i < json.issues.length) {
                  loopArray(json);
                } else {
                  parseCallback();
                }
              });
            }
            var that = this;
            var handleWorklog = function(issue, loopCallback) {
              console.log(issue.key);
              if (issue.fields.worklog !== undefined) {
                if (issue.fields.worklog.maxResults < issue.fields.worklog.total) {
                  // separate request to fetch all worklogs for issue, luckily it returns all worklogs so no need
                  // for recursion
                  var url = "/rest/api/2/issue/" + issue.key + "/worklog";
                  $.ajax({
                    url : url,
                    contentType : "application/json",
                    success : function(jsonWorklogs) {
                      // console.log("=== that._parseWorklogs in success");
                      that._parseWorklogs(dataParams, issue, jsonWorklogs.worklogs);
                      loopCallback();
                    },
                    error : function(jqXHR, textStatus, errorThrown) {
                      util.handleError(url, jqXHR, textStatus, errorThrown);
                    }
                  });
                } else {
                  // console.log("=== that._parseWorklogs in else");
                  that._parseWorklogs(dataParams, issue, issue.fields.worklog.worklogs);
                  loopCallback();
                }
              } else {
                // Carl Zeng reported that for some issues worklog is undefined. We skip such
                // issues.
                loopCallback();
              }
            }
            // start 'loop'
            loopArray(json);
          },

          // parses the worklogs json response and stores results in eventsArr
          _parseWorklogs : function(dataParams, issue, worklogsArr) {
            for (var j = 0; j < worklogsArr.length; j++) {
              var worklog = worklogsArr[j], startedDate = this._parseDate(worklog.started);
              // filter out by username and date
              if ((dataParams.allUsers || dataParams.usernames.indexOf(worklog.author.name) > -1)
                  && startedDate >= dataParams.start && startedDate < dataParams.end) {
                worklog.startedDate = startedDate;
                worklog.issueKey = issue.key;
                this.eventsArr.push(worklog);
              }
            }
          },

          _retrieveJSON : function(dataParams, callbackFun, startAt) {
            var jqlStr = "";
            if (this.jiraServerVersion !== undefined && this.jiraServerVersion >= "6.4") {
              jqlStr += "worklogDate >= "
                  + util.formatSystemDate(dataParams.start)
                  + " AND worklogDate < "
                  + util.formatSystemDate(dataParams.end)
                  + (!dataParams.allUsers ? " AND worklogAuthor in (" + util.concatenateArrQuoted(dataParams.usernames)
                      + ")" : "");
            } else {
              jqlStr += "updated > " + util.formatSystemDate(new Date(dataParams.start));
            }
            jqlStr += (dataParams.jql !== "" ? " AND " + dataParams.jql : "")
                + " AND timespent > 0&fields=summary,worklog&maxResults=" + this.maxResults
                + (startAt != null ? "&startAt=" + startAt : "");
            console.log("_retrieveJSON, jqlStr = " + jqlStr);
            var url = "/rest/api/2/search?jql=" + jqlStr;
            var that = this;
            $
                .ajax({
                  url : encodeURI(url),
                  contentType : "application/json",
                  success : function(data) {
                    that._parseJiraJSON(dataParams, data, function() {
                      console.log("that.total that.startAt that.maxResults " + that.total + " " + that.startAt + " "
                          + that.maxResults);
                      if (that.total - that.startAt > that.maxResults) {
                        console.log("recursion");
                        that._retrieveJSON(dataParams, callbackFun, that.startAt + that.maxResults);
                      } else {
                        console.log("callbackFun");
                        callbackFun();
                      }
                    });
                  },
                  error : function(jqXHR, textStatus, errorThrown) {
                    if (jqXHR != null && jqXHR.responseText != null) {
                      if (jqXHR.responseText.indexOf("by anonymous") > -1) {
                        util.showError("Looks like your JIRA session ended.<br/>Please <a href='" + contextPath
                            + "/login.jsp?permissionViolation=true'>Log in</a>");
                      } else if (jqXHR.responseText.indexOf("'project'") > -1) {
                        /**
                         * Probably it's "The value 'PROJ_KEY' does not exist for the field 'project'." error caused by
                         * luck of BROWSE permission. Try to handle it nicely.
                         */
                        util
                            .showError("Failed to retrieve issues.<br/>"
                                + "Looks like you do not have BROWSE permission for at least one project. "
                                + "This can happen when you are JIRA admin or Project admin and in the same time, you do not have "
                                + "'Browse Projects' permission for projects that you can access as admin. "
                                + "You need to grant 'Browse Project' permission to see reporting for these projects. "
                                + "<br/>You can find project keys in error returned by JIRA:<br/><br/>"
                                + jqXHR.responseText);
                      } else if (jqXHR.responseText.indexOf("'timespent'") > -1) {
                        /**
                         * Looks like Time Tracking is not enabled
                         */
                        util.showError("Looks like your JIRA Time Tracking is deactivated.<br/>"
                            + "Please contact your JIRA administrator.");
                      } else {
                        util.handleError(url, jqXHR, textStatus, errorThrown);
                      }
                    } else {
                      util.handleError(url, jqXHR, textStatus, errorThrown);
                    }
                  }
                });
          }

        }
      }; // var EventsProvider =
      var eventsProvider = new EventsProvider();
      return eventsProvider;
    });