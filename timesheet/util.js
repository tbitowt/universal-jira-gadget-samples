define('_ujgUtil', [ '_ujgJQuery' ], function($) {
  var Util = {

    monthNames : [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ],

    addDays : function(date, days) {
      return new Date(date.getTime() + (days * 24 * 60 * 60 * 1000));
    },

    concatenateArrQuoted : function(arrToConcatenate) {
      var result = '';
      for ( var index in arrToConcatenate) {
        result += '"' + arrToConcatenate[index].replace(/"/g, '\\"') + '"'
            + (index < (arrToConcatenate.length - 1) ? ',' : '');
      }
      return result;
    },

    formatDurationInHrs : function(seconds) {
      var results = "", HOUR = 3600;
      results = results + ((seconds / HOUR).toFixed(2));
      return results;
    },

    formatShortDate : function(dateToFormat) {
      return this.padStr(dateToFormat.getDate()) + "<br/>" + this.monthNames[dateToFormat.getMonth()];
    },

    formatSystemDate : function(dateToFormat) {
      var dateStr = dateToFormat.getFullYear() + "-" + this.padStr(1 + dateToFormat.getMonth()) + "-"
          + this.padStr(dateToFormat.getDate());
      return dateStr;
    },

    handleError : function(url, jqXHR, textStatus, errorThrown) {
      if (errorThrown === undefined) {
        if (jqXHR !== undefined && jqXHR.statusText !== undefined) {
          errorThrown = jqXHR.statusText;
        }
      }
      var status, errorMessages = "";
      if (jqXHR !== undefined) {
        status = jqXHR.status;
        // Check if there is our custom error text
        try {
          var json = $.parseJSON(jqXHR.responseText);
          if (json.errorMessages && json.errorMessages instanceof Array) {
            for (var i = 0; i < json.errorMessages.length; i++) {
              errorMessages += json.errorMessages[i] + " ";
            }
          }
          if (json.errors && typeof json.errors === 'object') {
            for ( var errorField in json.errors) {
              errorMessages += errorField + ": " + json.errors[errorField] + " ";
            }
          }
        } catch (e) {
        }
      }
      var reportError = 'URL: ' + url + (textStatus !== undefined ? ", Error: " + textStatus : "")
          + (errorThrown !== undefined ? ', ErrorThrown: ' + errorThrown : "")
          + (status !== undefined ? ', Status: ' + status : "")
          + (errorMessages !== "" ? ', ErrorMessages: ' + errorMessages : "");
      this.showError(reportError);
    },

    padStr : function(i) {
      return (i < 10) ? "0" + i : "" + i;
    },

    showError : function(text) {
      $("body").prepend("<div class='field-error'>" + text + "</div>");
    }

  };

  return Util;
});